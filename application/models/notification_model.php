<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Notification_model extends CI_Model {
    public function index() {

    }
    
    public function notification_save($type,$tablename)
    {
        $data = array(
                    'type' => $type,
                    'table_name' => $tablename,
                    'status' =>0
                     );
        $sql = "SELECT * from notification where type='$type' and table_name='$tablename' and status=0";
        $query = $this->db->query($sql);
        if ($query->num_rows() == 0) {
          $this->db->insert('notification', $data);
        }
        
    }
    
    public function get_notification()
    {
        $sql = "SELECT * from notification where  status=0";
        $query = $this->db->query($sql);
        if ($query->num_rows()> 0) {
           return $query->result_array();
        } else {
            return 0;
        }
    }
    public function update_status($id){
        $data1 = array(
            'status' =>1);
        $this->db->where('id', $id);
        $this->db->update('notification', $data1);
    }
    public function get_table_details($type,$tablename)
    {   
	
		date_default_timezone_set('America/Los_Angeles');
         
         //----------sending emails for super admin
        if($type==1){
		  $sql = "SELECT * from $tablename where email_status='0' ";
			   $query = $this->db->query($sql);
            	if ($query->num_rows()> 0) {
				  foreach ($query->result() as $row) {
				  	$emailid=$row->email_id;
						$userid=$row->id;
						$emailStatus=$row->email_status;
            $firstname=$row->firstname;
            $lastname=$row->lastname;
					  $this->load->model('doctor_model');
					  // $details=$this->doctor_model->mail_to_doctor($userid,$emailid,$emailStatus,$firstname,$lastname);
				  }
				  return true;
				}
				return false;
		}
        
    }
    
     //email configuration
    public function sendEmail($email,$subject,$bodycontent)
	{
        
        //----------------------------------------------
        //$ip=  base_url();
        $ip = $this->config->item('ip');
        $footerdata = $this->config->item('footer');
        $title = '<!DOCTYPE html>
                <html lang="en">
                <!-- Define Charset -->
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                <!-- Responsive Meta Tag -->
                <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
                <title>Email Template</title>';

        $css='<style type="text/css">
body {
	width: 100%;
	background-color: #F1F2F7;
	margin: 0;
	padding: 0;
	-webkit-font-smoothing: antialiased;
	font-family:"Open Sans", sans-serif;
}
table tr td{
font-family:"Open Sans", sans-serif;
-webkit-font-smoothing: antialiased;
}
html {
	width: 100%;
}
table {
	font-size: 14px;
	border: 0;
}
        /* ----------- responsive ----------- */
    @media only screen and (max-width: 640px) {
/*------ top header ------ */
.header-bg {
	width: 440px !important;
	height: 10px !important;
}
.main-header {
	line-height: 28px !important;
}
.main-subheader {
	line-height: 28px !important;
}
.container {
	width: 100% !important;
}
.container-middle {
	width: 420px !important;
}
.mainContent {
	width: 400px !important;
}
.main-image {
	width: 400px !important;
	height: auto !important;
}
.banner {
	width: 400px !important;
	height: auto !important;
}
/*------ sections ---------*/
.section-item {
	width: 400px !important;
}
.section-img {
	width: 400px !important;
	height: auto !important;
}
/*------- prefooter ------*/
.prefooter-header {
	padding: 0 10px !important;
	line-height: 24px !important;
}
.prefooter-subheader {
	padding: 0 10px !important;
	line-height: 24px !important;
}
/*------- footer ------*/
.top-bottom-bg {
	width: 420px !important;
	height: auto !important;
}
}
 @media only screen and (max-width: 479px) {
/*------ top header ------ */
.header-bg {
	width: 280px !important;
	height: 10px !important;
}
.top-header-left {
	width: 260px !important;
	text-align: center !important;
}
.top-header-right {
	width: 260px !important;
}
.main-header {
	line-height: 28px !important;
	text-align: center !important;
}
.main-subheader {
	line-height: 28px !important;
	text-align: center !important;
}
/*------- header ----------*/
.logo {
	width: 260px !important;
}
.nav {
	width: 260px !important;
}
.container {
	width: 280px !important;
}
.container-middle {
	width: 260px !important;
}
.mainContent {
	width: 240px !important;
}
.main-image {
	width: 240px !important;
	height: auto !important;
}
.banner {
	width: 240px !important;
	height: auto !important;
}
/*------ sections ---------*/
.section-item {
	width: 240px !important;
}
.section-img {
	width: 240px !important;
	height: auto !important;
}
/*------- prefooter ------*/
.prefooter-header {
	padding: 0 10px !important;
	line-height: 28px !important;
}
.prefooter-subheader {
	padding: 0 10px !important;
	line-height: 28px !important;
}
/*------- footer ------*/
.top-bottom-bg {
	width: 260px !important;
	height: auto !important;
}
}
</style>
<style type="text/css" charset="utf-8">

    /** reset styling **/

.firebugResetStyles {

    z-index: 2147483646 !important;

    top: 0 !important;

    left: 0 !important;

    display: block !important;

    border: 0 none !important;

    margin: 0 !important;

    padding: 0 !important;

    outline: 0 !important;

    min-width: 0 !important;

    max-width: none !important;

    min-height: 0 !important;

    max-height: none !important;

    position: fixed !important;

    transform: rotate(0deg) !important;

    transform-origin: 50% 50% !important;

    border-radius: 0 !important;

    box-shadow: none !important;

    background: transparent none !important;

    pointer-events: none !important;

    white-space: normal !important;
}
.firebugBlockBackgroundColor {

    background-color: transparent !important;
}
.firebugResetStyles:before, .firebugResetStyles:after {

    content: "" !important;

}
    /**actual styling to be modified by firebug theme**/

.firebugCanvas {

    display: none !important;

}
.firebugLayoutBox {

    width: auto !important;

    position: static !important;

}



.firebugLayoutBoxOffset {

    opacity: 0.8 !important;

    position: fixed !important;

}



.firebugLayoutLine {

    opacity: 0.4 !important;

    background-color: #000000 !important;

}



.firebugLayoutLineLeft, .firebugLayoutLineRight {

    width: 1px !important;

    height: 100% !important;

}



.firebugLayoutLineTop, .firebugLayoutLineBottom {

    width: 100% !important;

    height: 1px !important;

}



.firebugLayoutLineTop {

    margin-top: -1px !important;

    border-top: 1px solid #999999 !important;

}



.firebugLayoutLineRight {

    border-right: 1px solid #999999 !important;

}



.firebugLayoutLineBottom {

    border-bottom: 1px solid #999999 !important;

}



.firebugLayoutLineLeft {

    margin-left: -1px !important;

    border-left: 1px solid #999999 !important;

}



    /* ----------------- */

.firebugLayoutBoxParent {

    border-top: 0 none !important;

    border-right: 1px dashed #E00 !important;

    border-bottom: 1px dashed #E00 !important;

    border-left: 0 none !important;

    position: fixed !important;

    width: auto !important;

}



.firebugRuler{

    position: absolute !important;

}



.firebugRulerH {

    top: -15px !important;

    left: 0 !important;

    width: 100% !important;

    height: 14px !important;

    border-top: 1px solid #BBBBBB !important;

    border-right: 1px dashed #BBBBBB !important;

    border-bottom: 1px solid #000000 !important;

}



.firebugRulerV {

    top: 0 !important;

    left: -15px !important;

    width: 14px !important;

    height: 100% !important;

    border-left: 1px solid #BBBBBB !important;

    border-right: 1px solid #000000 !important;

    border-bottom: 1px dashed #BBBBBB !important;

}



.overflowRulerX > .firebugRulerV {

    left: 0 !important;

}



.overflowRulerY > .firebugRulerH {

    top: 0 !important;

}



    /* --------------------------------- */

.fbProxyElement {

    position: fixed !important;

    pointer-events: auto !important;

}

</style></head><body leftmargin="0" topmargin="0" marginheight="0" marginwidth="0">';
//header
$body_header='<table border="0" cellpadding="0" cellspacing="0" width="100%"><tbody>
    <tr>
      <td height="10"></td>
    </tr>
    <tr >
      <td align="center"  valign="top" width="100%">
	          <div style="width:600px; margin:0px auto; background:#7087A3;">
        <table class="container" align="center" border="0" cellpadding="0" cellspacing="0" width="600">
          <tbody>
            <tr bgcolor="7087A3">
              <td height="15"></td>
            </tr>
            <tr bgcolor="7087A3">
              <td align="center"><table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                  <tbody>
                    <tr>
                      <td><table class="top-header-left" align="left" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td align="center"><table class="date" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      <td><img  style="display: block;" src="' . $ip . '/img/email-img/icon-cal.png" alt="icon 1" width="13"></td>
                                      <td>&nbsp;&nbsp;</td>
                                      <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"><singleline> ' . date("d/m/Y") . '</singleline></td>
                                    </tr>
                                  </tbody>
                                </table></td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="top-header-right" align="left" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td height="20" width="30"></td>
                            </tr>
                          </tbody>
                        </table>
                        <table class="top-header-right" align="right" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
                              <td align="center">
							  <!--<table class="tel" align="center" border="0" cellpadding="0" cellspacing="0">
                                  <tbody>
                                    <tr>
                                      <td><img  style="display: block;" src="img/email-img/icon-tel.png" alt="icon 2" width="17"></td>
                                      <td>&nbsp;&nbsp;</td>
                                      <td style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"><singleline> Sales : +1 (222) 333-4444 </singleline></td>
                                    </tr>
                                  </tbody>
                                </table>-->
								</td>
                            </tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr bgcolor="7087A3">
              <td height="10"></td>
            </tr>
          </tbody>
        </table></div></td></tr>';
		
		//logo
$body_contenttop='<tr><td><table class="container" align="center"  border="0" cellpadding="0" cellspacing="0" width="600" bgcolor="ffffff"> <!--Header-->
<tbody><tr ><td height="40"></td></tr><tr><td><table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560"><tbody><tr><td><table class="logo" align="center" border="0" cellpadding="0" cellspacing="0">
                          <tbody>
                            <tr>
								<td align="center">
									<a href="#" style="display: block;"><img  style="display: block;" src="' . $ip . '/img/dashboardassets/email_logo.jpg" alt="logo" width="155">
									</a>
								</td>
                            </tr>
							<tr><td>&nbsp;</td></tr>
							<tr><td><b style="margin-top:5px;">ZATSA - Tool for Sports Administration</b></td></tr>
                          </tbody>
                        </table></td>
                    </tr>
                  </tbody>
                </table></td>
            </tr>
            <tr >
              <td height="40"></td>
            </tr>';

         
          $prefooter='
            <tr>
              <td height="20"></td>
            </tr>
            <tr>
              <td></td>
            </tr>
            <tr>
              <td height="30"></td>
            </tr>
            <tr>
              <td  style="color: #939393; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" class="prefooter-subheader" align="center">
             <span style="color: #7087A3">Website :</span> <a style="text-decoration:none;" href="www.zatsa.com">www.zatsa.com</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color: #7087A3">Email :</span><a style="text-decoration:none;"href="mailto:info@zatsa.com">info@zatsa.com<a>    
              
            </td></tr>
            <tr>
              <td height="30"></td>
            </tr>
          </tbody>
        </table>
        ';
//content Block
          $body_contenttop.=$bodycontent;


            $footer='</td></tr><tr><td>
                    <div style="width:600px; margin:0px auto; background:#7087A3; padding-top:15px; text-align:center; height:30px; color: #fff; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;" >

                  '.$footerdata.'
            </div>
            <!-- end footer--></td>
            </tr>

            </tbody>
            </table>
            </body>
            </html>';
        
        
         $message=$title.$css.$body_header.$body_contenttop.$prefooter.$footer;
        //----------------------------------------------
        
  // echo $message;
        
        $to=$email;
            //---------- Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            //---------- More headers
            $headers .= 'From: <no-reply@waafit.com>' . "\r\n";
           
            @mail($to,$subject,$message,$headers);
            
            return true;
                    } 

            $config = Array(
               'protocol' => 'smtp',
               'smtp_host' => 'ssl://smtp.gmail.com',
               'smtp_port' => 465,
               'smtp_user' => 'fitwaa@gmail.com', 
               'smtp_pass' => 'waafit123',
               'mailtype'  => 'html',
            'charset'=>'utf-8',
            'smtp_timeout'=>'30',
            'newline'=>'\r\n'
           );
         
          $this->load->library('email', $config);
          $this->email->set_newline("\r\n");
          $this->email->from('noreply@waafit.com');
          $this->email->to($email);
          $this->email->subject($subject);
          $this->email->message($message);
            if($this->email->send())
            {
                 return true;
            }
            else
            {
              return false;  
            }
	}
    
    
}