<?php

 if (!defined('BASEPATH')) exit('No direct script access allowed');

class Doctor_model extends CI_Model {

	public function index() {

	}

	//--save the doctor details
	public function save_doc($firstname,$lastname,$mobileno,$emailid,$pwd,$address,$address2,$lat,$lng)
	{
	
  $data = array(
                'firstname' => $firstname,
                'lastname' => $lastname,
                'email_id' => $emailid,
                'phone_no' => $mobileno,
                'password' => $pwd,
                'address' => $address,
                'address2' => $address2,
                'lat_lng' => $lat.','.$lng,
                 );
    //    print_r($data);
  //exit();
        $this->db->insert('doctors', $data);
        $this->notification_sendmail();

	}
	//-- send notification to send email
	public function notification_sendmail()
	{
		
		$type=1;
		$tablename='doctor';
    $this->load->model('notification_model');
		$this->notification_model->notification_save($type,$tablename); 
	}

	//--email setup
	public function mail_to_doctor($userid,$emailid,$emailStatus,$firstname,$lastname)
	{

      
        $ip=$this->config->item('ip');
        $subject = "Waa Fit-SIGN UP CONFIRMATION ";
        // $email="nitheeshtest@gmail.com";
        $email = $emailid;
        $footer = $this->config->item('footer');
        $sh_emailid = urlencode($emailid);
        $key = md5(mt_rand());
        $encr_user_id = $key . "|" . $userid . "|" . $sh_emailid;
         $bodycontent = '<tr>
              <td>
                    <table class="container-middle" align="center" border="0" cellpadding="0" cellspacing="0" width="560">
                      <tr>
                        <td><table class="mainContent" align="center" border="0" cellpadding="0" cellspacing="0" width="528">
                            <tbody>
                              <tr>
                                <td  class="main-header" style="color: #484848; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> 
                                <h2>SIGN UP CONFIRMATION - SPORTS ADMINISTRATOR</h2> 
                                </td>
                              </tr>
                              <tr>
                                <td height="20"></td>
                              </tr>
                              <tr>
                                <td  class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; line-height:20px; font-family: Helvetica, Arial, sans-serif;"> 
                                <b>Hi ' . $first_name . ' ' . $last_name . '</b>, 
                                </td>
                              </tr>
                              <tr>
                              <td>
                                        <p style="margin-bottom:10px;">You have been signed up as a Doctor Admin to the WAAFIT- platform
                                        </p>
                                        <p>Please click on the link below to activate your account.</p>
                              </td>
                                </tr>
                                <tr>
                                    <td>
                                    <p><a target="_blank" style="text-decoration:none;"  href="' . $ip . '/index.php/doctor/dcAccountActivate/' . $encr_user_id . '"><b>Click Here</b></a>
                                </p>
                                    </td>
                                </tr>
                                  <tr><td height="40"></td></tr>
                              <tr>
                                    <td>
                                     <table>
                                    <tr><td>Regards</td></tr>
                                    <tr><td><b>WAAFIT Team</b></td></tr>
                                    </table>
                                    </td>
                              </tr>
                            </tbody>
                          </table></td>
                      </tr>
                  
                    </table>
                    
                </td>
            </tr>';
        //echo $email.$subject.$message;
        $this->load->model('notification_model');
        $result = $this->notification_model->sendEmail($email, $subject, $bodycontent);
        if ($result == TRUE) {

            $data1 = array('status' => 1);
            $this->db->where('id', $userid);
            $this->db->update('doctors', $data1);

            return true;
        } else {
            return $result;
        }

		}
		
}

?>