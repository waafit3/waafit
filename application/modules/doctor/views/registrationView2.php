<?php $this->load->view('header');?>

<input type="hidden" id="fname" name="fname" value="<?php echo $doc_details['firstname'];?>">
<input type="hidden" id="lname" name="lname" value="<?php echo $doc_details['lastname'];?>">
<input type="hidden" id="mobno" name="mobno" value="<?php echo $doc_details['mobno'];?>">
<input type="hidden" id="emailid" name="emailid" value="<?php echo $doc_details['emailid'];?>">
   <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Waafit</a>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Doctor Registration-Fill in your details and we will get you started</h4>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form  id="doctordetails" class="formerror" method="POST">
                                        <div class="form-group ">
                                        	<label>Password</label>
                                            <input class="form-control" type="password" for="pwd" class="error" placeholder="" id="pwd" name="pwd">

                                        </div>
                                      	<div class="form-group ">
                                      		<label>Confirm Password</label>
                                            <input class="form-control" type="password" placeholder="" for="Cpwd" class="error" id="cpwd" name="cpwd">
                                        </div>
                                        <div class="form-group ">
                                            <label>Address1</label>
                                            <input class="form-control" placeholder="" for="address1" class="error"  id="address1" name="address1">
                                        </div>
                                        <div class="form-group ">
                                        	<label>Street/Town/City</label>
                                            <input class="form-control" placeholder="" for="" class="error"  id="address" name="address">
                                        </div>
                                        <!-- ------------------------------------------ -->

                                        <div class="form-group">
                            <div class="col-md-1 col-sm-1 col-lg-1  pull-right" style="  display: inline-block;vertical-align: middle;margin-left: 0px;  float: right !important;  padding: 0px;">
                            <?php if ($v_id!= "") { ?><h2 style="text-align:right;"><a onclick="fn_checkdt();" title="Edit current place" class=""><i class="fa fa-edit lead" style="font-size:40px;"></i></a> </h2><?php }?>
                          </div>
                        </div>
                                  <div class="clearfix"></div>
                                 <div class="form-group"  style="display:none">
                                      <button type="button" class="btn btn-primary btn-md" id="find" >Search</button>
                                  </div>
                                   <div class="clearfix"></div>
                                   <div class="form-group" style="" >
                                   <label for="Latitude" class="label1">Latitude</label> 
                                   <input type="text" disabled class="form-control" style="display:block !important;" id="lat"  name="lat" value="12.9667" >
                                  </div>
                                   <div class="form-group"  style="" >
                                    <label for="Longitude" class="label1">Longitude</label>
                                <input type="text" disabled class="form-control" id="lng" style="display:block !important;" name="lng"  value="77.5667" >
                                      <div id="chk_geo_loc"></div>
                                      
                                  </div>   
                                </form>
                                <div class="clear"></div>
                                        <!-- ------------------------------------------ -->
                                        <button onclick="save()" class="btn btn-primary">SignUp</button>
                                </div>
                                 <div align="center" id="map" class="map_canvas" style="height: 420px;border:1px solid lightgrey;margin:10px;padding:10px;" ><br/>
                                 </div>
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
<?php $this->load->view('footer');?>
 <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
 <script src="<?php echo base_url(); ?>js/geocomplete/jquery.geocomplete.js?09865678987"></script>
 <script src="<?php echo base_url(); ?>js/geocomplete/logger.js"></script>
<script>
  $().ready(function() {


  	 $.validator.addMethod("chk_pwd", function(value, element) {
       var pwrd=$('#pwd').val();
     if(value==pwrd)
     {
        return 'true'; 
     }
    }, "Name must contain only letters");

  $("#doctordetails").validate({
       rules: {
        'pwd':{required: true},
        'cpwd':{required: true,chk_pwd:true},
        'address1':{required: true},
        'address':{required: true}
        },messages: {
        'pwd': { required: 'Please enter password<br>'},
        'cpwd': { required: 'Please enter confirm password<br>',chk_pwd:'Password does not match'},
        'address1': { required:'Please enter address1'},
        'address': { required:'Please enter address'}
        }
    
    });
});

//onclick the clinic details button
        function save(){
         // if( $("#doctordetails").valid()){
              var fname=$('#fname').val();
              var lname=$('#lname').val();
              var mobno=$('#mobno').val();
              var emailid=$('#emailid').val();
              var address1=$('#address1').val();
              var address=$('#address').val();
              var lat=$('#lat').val();
              var lng=$('#lng').val();
                var pwd=$('#pwd').val();
                var string_array = "&fname=" +fname+"&lname=" + lname +"&mobno=" + mobno + "&emailid=" + emailid + "&pwd=" + pwd + "&address1=" + address1 + "&address=" + address + "&lat=" + lat +"&lng=" + lng ;
                          alert(string_array);
                                $.ajax({
                                        type: "POST",
                                        url: "<?php echo site_url('doctor/doctor_prof_add'); ?>",
                                        data: string_array,
                                        success: function(msg) {
                                            alert(msg);
                                            //  alert(msg);
                                             // console.log(msg);
                                              //window.location.href = '<?php //echo site_url('school/school_profile/view'); ?>' + '/' + x + '_' + typeid;
                                        }
                                });

                   
         }
  
</script>
    <script src="http://localhost/waafit/js/jquery.validate.min.js"></script>
    <style>
    .error {
        font-size:13px;
        font-weight: thin;
        color: red;
    }
    </style>

    <script>
        function fn_checkdt(){
        var r = confirm("Are you sure need to edit the location?");
        if (r == true) {
        
             $("#address").prop('disabled',false);
            //google.maps.event.removeDomListener(window, 'load', initialize);
            //$("#map").html("");
            //$("#map").empty();
            GetAddress();
            //$("#map").unbind( "click" );
        } 
    }
        function GetAddress() {
            var lat = document.getElementById("lat").value;
            var lng = document.getElementById("lng").value;
            var latlng = new google.maps.LatLng(lat, lng);
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {
                     $("#address").val(results[0].formatted_address);
                      $("#find").click();
                    }
                }
            });
        }
    
 $(function(){
     if(document.getElementById("lat").value!='' || document.getElementById("lat").value!=0 ){
        //GetAddress();
        initialize();
    // google.maps.event.addDomListener(window, 'load', initialize);
     }
    
       var options = {
          map: ".map_canvas"
        };
       
            
        $("#address").geocomplete(options)
        /*
       $("#find").click();
        */
      });
      
        $("#find").click(function(){
            $("#address").trigger("geocode");
        });
            function initialize() {
            var myLatlng = new google.maps.LatLng(document.getElementById("lat").value,document.getElementById("lng").value);

            var myOptions = {
            zoom:14,
            center: myLatlng
            }
            map = new google.maps.Map(document.getElementById("map"), myOptions);
            var marker = new google.maps.Marker({
            draggable:true,
            position: myLatlng, 
            map: map,

            });
     }
    
    </script>