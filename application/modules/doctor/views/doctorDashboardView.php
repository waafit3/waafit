<?php $this->load->view('header');?>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Waafit</a>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header"></h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Doctor Registration
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <form role="form" id="doctordetails" class="formerror" method="POST" action="<?php echo site_url('doctor/details');?>">
                                       
                                        <div class="form-group ">
                                            <label>First Name</label>
                                            <input class="form-control" for="firstname" class="error" placeholder="" id="firstname" name="firstname">

                                        </div>
                                      	<div class="form-group ">
                                            <label>Last Name</label>
                                            <input class="form-control" placeholder="" for="lastname" class="error" id="lastname" name="lastname">
                                        </div>
                                      
                                        <div class="form-group ">
                                            <label>Mobile Number</label>
                                            <input class="form-control" placeholder="" for="mobno" class="error"  id="mobno" name="mobno">
                                        </div>
                                        <div class="form-group ">
                                            <label>Email ID</label>
                                            <input class="form-control" placeholder="" for="emailid" class="error"  id="emailid" name="emailid">
                                        </div>
                                        <button type="submit" class="btn btn-primary">Continue To Clinic Details</button>
                                </div>
                              
                            </div>
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?php $this->load->view('footer');?>
<script>
  $().ready(function() {
  $("#doctordetails").validate({
       rules: {
        'firstname':{required: true},
        'lastname':{required: true},
        'emailid':{required: true,email:true},
          'mobno':{required: true,number:true,maxlength:12}
        },messages: {
        'firstname': { required: 'First name is required<br>'},
        'lastname': { required: 'Last name is required<br>'},
        'emailid': { required: 'Email is required<br>',email:"Please enter valid email!\n",remote:"Email id already axist!\n"},
         'mobno': { required:'Please enter contact no!<br>',number:'Only numbers allowed\n'}
        }
    
    });
});

//onclick the clinic details button
        function clickdetails(){
         if(staff_form.valid()){


                }   
         }
  
</script>
    <script src="http://localhost/waafit/js/jquery.validate.min.js"></script>
    <style>
    .error {
        font-size:13px;
        font-weight: thin;
        color: red;
    }
    </style>