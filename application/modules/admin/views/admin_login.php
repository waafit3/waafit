<?php $this->load->view('header'); ?>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sign In Now</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" action="admin/check_user" onsubmit="return test()" method="POST">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" id="usermail" name="usermail" placeholder="E-mail" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" id="password" name="password" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <!--<a href="#" class="btn btn-lg btn-success btn-block">Login</a>-->   
                                <input type="submit" value="submit">
<!-- 
                                <button class="btn btn-lg btn-success btn-block" type="submit">Login</button> -->
                            </fieldset>
                        </form>
                        <div id="error" style="color:red;"><?php echo $error; ?></div>
                    </div>                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('footer'); ?>
<script>
          function test()
          {
            alert("test");
          }
            //validation
            function validation()
            {
              alert("tester");
                var usermail = document.getElementById("usermail").value;
                var password = document.getElementById("password").value;
                var atpos=usermail.indexOf("@");
                var dotpos=usermail.lastIndexOf(".");
                if (usermail == '')
                {
                    document.getElementById("error").innerHTML = "<span class='loginError'>Please enter your username</span>";
                    return false;
                }
                if (atpos<1 || dotpos<atpos+2 || dotpos+2>=usermail.length)
        {
            document.getElementById("error").innerHTML = "<span  class='loginError'>Please enter valid email id</span>";
            return false;
        }
                if (password == '')
                {
                    document.getElementById("error").innerHTML = "<span class='loginError'>Please enter your password</span>";
                    return false;
                }
            }
            
        //check email exist or not
        function checkemail()
    {
            $("#loader_ajax").show();
    var email=document.getElementById("emailid").value; 
        var atpos=email.indexOf("@");
        var dotpos=email.lastIndexOf(".");
        document.getElementById("msg").innerHTML = "";
            if(email=='')
            {
                    $("#loader_ajax").hide();
                    document.getElementById("msg").innerHTML = "<span class='loginError'>Please enter your email id</span>";
                    return false
            }
            else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length)
            {
                    $("#loader_ajax").hide();
                    document.getElementById("msg").innerHTML = "<span class='loginError'>Enter valid email id</span>";
                    return false;
            }
            else
            {
            $.ajax({
                    type: "post",
                    url: "<?php echo site_url('login/check_email');?>",
                    data:{email:email},
                    beforeSend: function(){
                    $("#loader_ajax").show();
                    },
                    success: function(msg)
                    {
                        if(msg==1)
                            { 
                                $("#loader_ajax").hide();
                            //document.getElementById("msg").innerHTML = "<span class='form-control' style='border:0px;color:green'>Available</span>";

                            }
                            else
                            { 
                                $("#loader_ajax").hide();
                                document.getElementById("msg").innerHTML = "<span class='loginError'>Your email id does not exist</span>";
                                document.getElementById("email").value="";
                                return false;
                            }
                            $("#loader_ajax").hide();
                    }
                    });
            }
        }
        
     
    </script>